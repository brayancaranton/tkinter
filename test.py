from pyfirmata import Arduino, util
from tkinter import *
from PIL import Image , ImageTk
import time
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

window_width=600
window_height=1080
canvas_width =  1900
canvas_height = 980
sizex_circ=60
sizey_circ=60
width = 150
height = 150
placa = Arduino ('/dev/cu.usbmodem1421')
it = util.Iterator(placa)

#inicio el iteratodr
it.start()

content=0
prom1=0
prom2=0
datos1=[]
datos2=[]
valor1=0
valor2=0

led1= placa.get_pin('d:9:p') 
led2= placa.get_pin('d:10:p') 

pot1= placa.get_pin('a:0:i')
pot2= placa.get_pin('a:1:i')

ventana = Tk()
ventana.geometry('400x320')
ventana.configure(bg = 'white')
ventana.title("PARCIAL FINAL")
draw = Canvas(ventana, width=canvas_width, height=canvas_height)
draw.place(x = 10,y = 10)


# Fetch the service account key JSON file contents
cred = credentials.Certificate('/Users/yeny/Desktop/base/secreto/llave.json')
# Initialize the app with a service account, granting admin privileges
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://databasequiz-a7b01.firebaseio.com/'})

Label(ventana, text="SISTEMA DE CONTROL").place(x=40, y=10)
Label(ventana, text="BRAYAN CARANTON").place(x=45, y=30)
led1_draw=draw.create_oval(150,200,150+sizex_circ,200+sizey_circ,fill="white")
Label(ventana, text="LED1").place(x=167, y=275)
led2_draw=draw.create_oval(250,200,250+sizex_circ,200+sizey_circ,fill="white")
Label(ventana, text="LED2").place(x=267, y=275)

def medir():
    global content, valor1, valor2, prom1, prom2, datos1, datos2
    ref=db.reference("adc")
    valor1=pot1.read()
    valor2=pot2.read()
    dato1['text'] = str(valor1)
    dato2['text'] = str(valor2)
    
    if content== "i":
        valor1=pot1.read()
        valor2=pot2.read()
        led1.write(int(valor1))
        led2.write(int(valor2))
        
        if(int(valor1)>0.9):
            draw.itemconfig(led1_draw, fill="blue3")       
        if(int(valor1)>0.8 and int(valor1)<=0.9):
            draw.itemconfig(led1_draw, fill="blue2")
        if(int(valor1)>0.6 and int(valor1)<=0.8):
            draw.itemconfig(led1_draw, fill="blue1")
        if(int(valor1)>0.4 and int(valor1)<=0.6):
            draw.itemconfig(led1_draw, fill="SkyBlue3")
        if(int(valor1)>0.2 and int(valor1)<=0.4):
            draw.itemconfig(led1_draw, fill="SkyBlue2")
        if(int(valor1)<=0.2):
            draw.itemconfig(led1_draw, fill="SkyBlue1")

        if(int(valor2)>0.9):
            draw.itemconfig(led2_draw, fill="blue3")
        if(int(valor2)>0.8 and int(valor2)<=0.9):
            draw.itemconfig(led2_draw, fill="blue2")
        if(int(valor2)>0.6 and int(valor2)<=0.8):
            draw.itemconfig(led2_draw, fill="blue1")
        if(int(valor2)>0.4 and int(valor2)<=0.6):
            draw.itemconfig(led2_draw, fill="SkyBlue3")
        if(int(valor2)>0.2 and int(valor2)<=0.4):
            draw.itemconfig(led2_draw, fill="SkyBlue2")
        if(int(valor2)<=0.2):
            draw.itemconfig(led2_draw, fill="SkyBlue1")            

    if content== "p":
        for i in range (10):
            valor1=pot1.read()
            valor2=pot2.read()
            prom1=valor1+prom1
            datos1.append(valor1)
            prom2=valor2+prom2
            datos2.append(valor2)
            time.sleep(0.5)
        prom1=prom1/10
        prom2=prom2/10
        print(prom1,prom2)

    if content == "g":
            ref.update({
        'adc1':{
            'valor':valor1,
            'promedio':prom1
            },
        'adc2':{
            'valor':valor2,
            'promedio':prom2
            }})

    ventana.after(500,medir)


def entrada(input):
    global content
    content = dato.get()
    dato.delete(0, END)

dato1=Label(ventana, text="0.0", bg='cadet blue2', font=("Arial Bold", 14), fg="white")
dato1.place(x=35, y=90)
Label(ventana, text="ADC1").place(x=27, y=115)
dato2=Label(ventana, text="0.0", bg='cadet blue2', font=("Arial Bold", 14), fg="white")
dato2.place(x=100, y=90)
Label(ventana, text="ADC2").place(x=92, y=115)
  
dato = Entry(ventana)
dato.place(x=30, y=150)
dato.bind('<Return>',entrada)
Label(ventana, text="FUNCIONES").place(x=27, y=185)
Label(ventana, text="· i: intensidad").place(x=27, y=210)
Label(ventana, text="· p: promedio").place(x=27, y=230)
Label(ventana, text="· g: guardar").place(x=27, y=250)

img=Image.open("/Users/yeny/Downloads/logousa.png")
img= img.resize((width,height))
imagen=ImageTk.PhotoImage(img)
b=Label(ventana,image=imagen).place(x=230,y=50)        



medir()

ventana.mainloop()



